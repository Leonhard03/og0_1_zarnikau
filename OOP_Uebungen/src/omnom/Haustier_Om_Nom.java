package omnom;

public class Haustier_Om_Nom {
    
    private int hunger;
    private int muede;
    private int zufriedenheit;
    private int gesundheit;
    private String name;

    
    public Haustier_Om_Nom() {
    }

    public Haustier_Om_Nom(String name) {
    	
      
    	name = name;
        hunger = 100;
        muede = 100;
        zufriedenheit = 100;
        gesundheit = 100;
    }

    
    public void fuettern(int futtern) {
        hunger += futtern;
        if (hunger > 100) {
            hunger = 100;
        }
    }
    
    public void schlafen(int schlafen) {
        muede += schlafen;
        if (muede > 100) {
            muede = 100;
        }
    }
    

    public void spielen(int spielen) {
        zufriedenheit += spielen;
        if (zufriedenheit > 100) {
            zufriedenheit = 100;
        }
    }
    
    public void heilen() {
        gesundheit = 100;
    }

    
    // -------
    
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
        
    }

    public int getHunger() {
        return hunger;
        
    }

    public void setHunger(int hunger) {
        if (!(hunger < 0 && hunger > 100)) {
            this.hunger = hunger;
        }
    }

    public int getMuede() {
        return muede;
    }

    public void setMuede(int muede) {
        if (!(muede < 0 && muede > 100)) {
           this.muede = muede;
        }
    }

    public int getZufrieden() {

        return zufriedenheit;
    }

    public void setZufrieden(int zufrieden) {
        if (!(zufrieden < 0 && zufrieden > 100)) {
            this.zufriedenheit = zufrieden;
        }
    }

    public int getGesundheit() {
        return gesundheit;
    }

    public void setGesundheit(int gesund) {
        if (!(gesund < 0 && gesund > 100)) {
            this.gesundheit = gesund;

        }

    }
}

public class Addon {

	private int idNummer;
	private String bezeichnung;
	private double verkaufswert;
	private int bestand;
	private int spielerBestand;
	private int maxBestand;


	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getVerkaufswert() {
		return verkaufswert;
	}

	public void setVerkaufswert(double verkaufswert) {
		this.verkaufswert = verkaufswert;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public int getSpielerBestand() {
		return spielerBestand;
	}

	public void setSpielerBestand(int spielerBestand) {
		this.spielerBestand = spielerBestand;
	}

	public int getMaxBestand() {
		return maxBestand;
	}

	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}

	public int spielerBestand() {
		return bestand;
	}

	public int veraenderterBestand() {
		return bestand;
	}

	public double erfasterGesamtwert() {
		return bestand;
	}

}


public class Trainer extends Person {

	public double entschaedigung;
	public char lizensklasse;
	
	public Trainer() {}

	public double getEntschaedigung() {
		return entschaedigung;
	}

	public void setEntschaedigung(double entschaedigung) {
		this.entschaedigung = entschaedigung;
	}

	public char getLizensklasse() {
		return lizensklasse;
	}

	public void setLizensklasse(char lizensklasse) {
		this.lizensklasse = lizensklasse;
	}
	
	
	
	
}


public class Spieler extends Person {

	public String spielpostion;
	public int trikotnummer;

	Spieler() {}

	public String getSpielpostion() {
		return spielpostion;
	}

	public void setSpielpostion(String spielpostion) {
		this.spielpostion = spielpostion;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}
	
	
}

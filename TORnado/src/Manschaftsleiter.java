
public class Manschaftsleiter extends Spieler {

	public String manschaftsname;
	public int rabatt;
	
	public Manschaftsleiter() {}
	
	public String getManschaftsname() {
		return manschaftsname;
	}
	public void setManschaftsname(String manschaftsname) {
		this.manschaftsname = manschaftsname;
	}
	public int getRabatt() {
		return rabatt;
	}
	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
	
	
}
